# -*- coding: utf-8 -*-

import sys
from PySide.QtCore import *
from PySide.QtGui import *


class Coder(object):

    def get_encode_text(self,source_text):
        """Method for encode source text(unicode) to MyCode.
        input: str from unicode
        output: str with encoded text"""

        list_of_int = self._str_to_int(source_text)
        list_of_bin = self._int_to_bin(list_of_int)
        list_of_mybin = self._bin_to_mybin(list_of_bin)
        list_of_new_int = self._bin_to_int(list_of_mybin)
        encode_text = self._int_to_str(list_of_new_int)
        return encode_text


    def get_decode_text(self,encode_text):
        """ input:  str with encoded text
            output: str from unicode with true(decoded) text"""
        list_of_int = self._str_to_int(encode_text)
        list_of_bin = self._int_to_bin(list_of_int)
        list_of_bin_new = self._mybin_to_bin(list_of_bin)
        list_of_new_int = self._bin_to_int(list_of_bin_new)
        decode_text = self._int_to_str(list_of_new_int)
        return decode_text

# functions for all text
    def _str_to_int(self,text):
        """ input:  str
            output: list of int"""
        list_of_int = []
        for chr in text:
            list_of_int.append(ord(chr))
        return list_of_int

    def _int_to_bin(self,list_of_int):
        """ input:  list of int
            output: list of bin with base 16 bit"""
        list_of_bin = []
        for i in list_of_int:
            list_of_bin.append(bin(i)[2:].zfill(16))
        return list_of_bin

    def _bin_to_int(self,list_of_bin):
        """ input:  list of bin
            output: list of int"""
        list_of_int = []
        for i in list_of_bin:
            list_of_int.append(int('0b%s' % i,2))
        return list_of_int

    def _int_to_str(self,list_of_int):
        """ input:  list of int
            output: str with true text"""
        result_text = ''
        for i in list_of_int:
            result_text += unichr(i)
        return result_text

    def _bin_to_mybin(self,list_of_bin):
        """ input:  list of bin with source text
            output: list of bin with encoded text (to my code)"""
        list_of_mybin = []
        list_of_bin = self._bin_reverse(list_of_bin)
        last_bit = None
        str_bm_new = ''   #new middle (next)
        str_br = ''   #result
        if list_of_bin:
            str_bm = list_of_bin.pop(0)   #middle
            if list_of_bin:
                for i_list, v_list in enumerate(list_of_bin):
                    for i_str, v_str in enumerate(str_bm):
                        if i_list == 0:
                            if i_str < 15:
                                bit = self._sum2(v_str,str_bm[i_str + 1])
                                str_br += bit
                                if i_str == 0:
                                    str_br = self._sum2(bit,v_str) + str_br #0
                                    last_bit = str_bm[15]
                                str_bm_new += self._sum2(bit,v_list[i_str])
                        else:
                            if i_str == 0:
                                bit = self._sum2(last_bit,v_str)
                                str_br += bit
                                last_bit = self._sum2(bit,list_of_bin[i_list - 1][15])
                                bit_1 = self._sum2(v_str,str_bm[i_str + 1])
                                str_br += bit_1
                                str_bm_new += self._sum2(bit_1,v_list[i_str])
                            elif i_str != 14:
                                bit = self._sum2(v_str,str_bm[i_str + 1])
                                str_br += bit
                                str_bm_new += self._sum2(bit,v_list[i_str])
                            elif i_str == 14:
                                bit = self._sum2(last_bit,v_str)
                                str_br += bit
                                str_bm_new += self._sum2(bit,v_list[i_str])
                    str_bm = str_bm_new
                    str_bm_new = ''
                    list_of_mybin.append(str_br)
                    str_br = ''
                for i_str, v_str in enumerate(str_bm):
                    if i_str == 0 or i_str == 14:
                        bit = self._sum2(last_bit,v_str)
                        str_br += bit
                        if i_str != 14:
                            last_bit = self._sum2(bit,list_of_bin[len(list_of_bin) - 1][15])
                            bit_1 = self._sum2(v_str,str_bm[i_str + 1])
                            str_br += bit_1
                    elif i_str != 14:
                        bit = self._sum2(v_str,str_bm[i_str + 1])
                        str_br += bit
            else:
                for i_str, v_str in enumerate(str_bm):
                    if i_str < 15:
                        bit = self._sum2(v_str,str_bm[i_str + 1])
                        str_br += bit
                        if i_str == 0:
                            str_br = self._sum2(bit,v_str) + str_br #0
            list_of_mybin.append(str_br)
        list_of_mybin = self._bin_reverse(list_of_mybin)
        return list_of_mybin

    def _mybin_to_bin(self,list_of_mybin):
        """ input:  list of bin with encoded text (from my code)
            output: list of bin with source text (decode to source-true text)"""
        list_of_bin = self._bin_reverse(list_of_mybin)
        list_of_mybin = []
        str_br = ''   #result
        str_bm = ''
        pre_bit = None
        for i_list, v_list in enumerate(list_of_bin):
            for i_str, v_str in enumerate(v_list):
                if i_list == 0:
                    if i_str == 0:
                        bit = self._sum2(v_str,v_list[1])
                        str_br += bit                                         #0
                        str_br += self._sum2(v_list[1],bit)                   #1
                    elif i_str > 1:
                        bit = str_br[len(str_br) - 1]
                        bit = self._sum2(v_str,bit)
                        str_br += bit
                else:
                    if not pre_bit:
                        pre_bit = list_of_mybin[i_list - 1][15]
                    if i_str == 0:
                        bit = self._sum2(v_str,pre_bit)
                        str_bm += bit
                        new_bit = self._sum2(bit,list_of_bin[i_list - 1][i_str + 1])
                    elif i_str < 15:
                        bit = self._sum2(str_bm[i_str - 1],v_str)
                        str_bm += bit
                        new_bit = self._sum2(bit,list_of_bin[i_list - 1][i_str + 1])
                    else:                                             # i_str = 15
                        bit = self._sum2(str_bm[i_str - 1],v_str)
                        str_bm += bit
                        new_bit = self._sum2(bit,v_list[0])
                    str_br += new_bit
            list_of_mybin.append(str_br)
            str_br = ''
            if str_bm != '':
                pre_bit = str_bm[15]
            str_bm = ''
        list_of_mybin = self._bin_reverse(list_of_mybin)
        return list_of_mybin

    def _bin_reverse(self,list_of_bin):
        """ 1010  --->  0101 """
        new_list_of_bin = []
        for i in list_of_bin:
            new_str = ''
            for j in i:
                new_str = j + new_str
            new_list_of_bin.append(new_str)
        return new_list_of_bin

    def _sum2(self,a,b):
        """ a,b - char '0' or '1'.
            result = sum by mod2."""
        if a == b:
            result = '0'
        else:
            result = '1'
        return result

class Form(QWidget):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        # Create widgets
        self.title_view = QLabel("View text letter by letter:")
        self.source_text = QTextEdit()
        self.encode_text = QTextEdit()
        self.decode_text = QTextEdit()
        self.label1 = QLabel("Source text:")
        self.encode_but = QPushButton ("Encode")
        self.label3 = QLabel("Encoded text:")
        self.decode_but = QPushButton ("Decode")
        self.label5 = QLabel("Decoded text:")
        self.exit_but = QPushButton("Exit")
        self.clear_all_but = QPushButton("Clear all")
        self.rank_drop = QComboBox()
        self.rank_drop.addItems([str(i) for i in xrange(16)])
        self.rank_drop.setToolTip('Enter a rank for feedback')

        self.labelRight1 = QLabel("Source letter")
        self.source_letter_s = QLineEdit()
        self.labelRight2 = QLabel("Unicode")
        self.source_letter_u = QLineEdit()
        self.labelRight3 = QLabel("Binary Unicode")
        self.source_letter_b = QLineEdit()

        self.labelRight6 = QLabel("Encoded letter")
        self.encode_letter_s = QLineEdit()
        self.labelRight7 = QLabel("Unicode")
        self.encode_letter_u = QLineEdit()
        self.labelRight8 = QLabel("Binary Unicode")
        self.encode_letter_b = QLineEdit()

        self.labelRight4 = QLabel("View")
        self.labelRight5 = QLabel("letter in a row")
        self.number_letter = QLineEdit()

        self.labelmovetext = QLabel("Movement of the source text")
        self.move_right = QPushButton("----->")
        self.move_left = QPushButton("<-----")

        # Create layout and add widgets
        mainLayout =  QHBoxLayout()
        leftLayout = QVBoxLayout()
        rightTopLayout = QVBoxLayout()
        rightLowLayout = QHBoxLayout()
        leftLayout1 = QHBoxLayout()
        leftLayout2 = QHBoxLayout()
        leftLayout3 = QHBoxLayout()
        leftLayout4 = QHBoxLayout()
        leftLayout5 = QHBoxLayout()

        rightLayout1 = QVBoxLayout()
        rightLayout2 = QVBoxLayout()
        rightLayout3 = QVBoxLayout()
        rightLayout4 = QHBoxLayout()
        rightLayout5 = QHBoxLayout()
        rightLayout6 = QVBoxLayout()
        rightLayout7 = QVBoxLayout()
        rightLayout8 = QVBoxLayout()
        rightLayout9 = QHBoxLayout()
        rightLayout10 = QHBoxLayout()
        rightLayout11 = QHBoxLayout()
        rightLayout12 = QHBoxLayout()
        rightLayout13 = QVBoxLayout()

        #Add widgets to layout
        rightLayout1.addWidget(self.labelRight1)
        rightLayout1.addWidget(self.source_letter_s)

        rightLayout2.addWidget(self.labelRight2)
        rightLayout2.addWidget(self.source_letter_u)

        rightLayout3.addWidget(self.labelRight3)
        rightLayout3.addWidget(self.source_letter_b)

        rightLayout4.addLayout(rightLayout1)
        rightLayout4.addLayout(rightLayout2)
        rightLayout4.addLayout(rightLayout3)

        rightLayout6.addWidget(self.labelRight6)
        rightLayout6.addWidget(self.encode_letter_s)

        rightLayout7.addWidget(self.labelRight7)
        rightLayout7.addWidget(self.encode_letter_u)

        rightLayout8.addWidget(self.labelRight8)
        rightLayout8.addWidget(self.encode_letter_b)

        rightLayout9.addLayout(rightLayout6)
        rightLayout9.addLayout(rightLayout7)
        rightLayout9.addLayout(rightLayout8)

        rightLayout5.addStretch()
        rightLayout5.addWidget(self.labelRight4)
        rightLayout5.addWidget(self.number_letter)
        rightLayout5.addWidget(self.labelRight5)
        rightLayout5.addStretch()

        rightLayout11.addStretch()
        rightLayout11.addWidget(self.labelmovetext)
        rightLayout11.addStretch()
        rightLayout12.addWidget(self.move_left)
        rightLayout12.addWidget(self.move_right)

        rightLayout13.addLayout(rightLayout11)
        rightLayout13.addLayout(rightLayout12)

        leftLayout1.addWidget(self.label1)
        leftLayout1.addStretch()

        leftLayout2.addStretch()
        leftLayout2.addWidget(self.encode_but)


        leftLayout3.addWidget(self.label3)
        leftLayout3.addStretch()

        leftLayout4.addStretch()
        leftLayout4.addWidget(self.decode_but)

        leftLayout5.addWidget(self.label5)
        leftLayout5.addStretch()

        leftLayout.addLayout(leftLayout1)
        leftLayout.addWidget(self.source_text)
        leftLayout.addLayout(leftLayout2)
        leftLayout.addLayout(leftLayout3)
        leftLayout.addWidget(self.encode_text)
        leftLayout.addLayout(leftLayout4)
        leftLayout.addLayout(leftLayout5)
        leftLayout.addWidget(self.decode_text)

        rightLayout10.addStretch()
        rightLayout10.addWidget(self.title_view)
        rightLayout10.addStretch()

        rightTopLayout.addLayout(rightLayout10)
        rightTopLayout.addLayout(rightLayout4)
        rightTopLayout.addStretch()
        rightTopLayout.addLayout(rightLayout5)
        rightTopLayout.addStretch()
        rightTopLayout.addLayout(rightLayout9)
        rightTopLayout.addStretch()
        rightTopLayout.addLayout(rightLayout13)
        rightTopLayout.addStretch()

        rightLowLayout.addStretch()
        rightLowLayout.addWidget(self.rank_drop)
        rightLowLayout.addWidget(self.clear_all_but)
        rightLowLayout.addWidget(self.exit_but)
        rightTopLayout.addLayout(rightLowLayout)
        mainLayout.addLayout(leftLayout)
        mainLayout.addLayout(rightTopLayout)
        # Set widget layout
        self.setLayout(mainLayout)
        self.setWindowTitle('Code Machine')
        self.decode_text.setReadOnly(True)
        self.encode_text.setReadOnly(True)
        self.source_letter_s.setReadOnly(True)
        self.source_letter_u.setReadOnly(True)
        self.source_letter_b.setReadOnly(True)
        self.encode_letter_s.setReadOnly(True)
        self.encode_letter_u.setReadOnly(True)
        self.encode_letter_b.setReadOnly(True)
        self.number_letter.setReadOnly(True)
        #Other variables
        self.source_text_g_var = ''
        self.current_letter = 0
        self.coder = Coder()
        # Add button signal to greetings slot
#        self.buttonExit.clicked.connect(sys.exit)
        self.encode_but.clicked.connect(self.encode)
        self.decode_but.clicked.connect(self.decode)
        self.clear_all_but.clicked.connect(self.clear_all)


    #Methods
    def encode(self):
        s_text = self.source_text.toPlainText()
        e_text = self.coder.get_encode_text(s_text)
        self.encode_text.setPlainText(e_text)

    def decode(self):
        e_text = self.encode_text.toPlainText()
        if e_text:
            d_text = self.coder.get_decode_text(e_text)
        else:
            d_text = ''
        self.decode_text.setPlainText(d_text)

#    def move_to_left(self):

#    def move_right(self):

    def clear_all(self):
        self.source_text.setPlainText('')
        self.decode_text.setPlainText('')
        self.encode_text.setPlainText('')
        self.source_letter_s.setText('')
        self.source_letter_u.setText('')
        self.source_letter_b.setText('')
        self.encode_letter_s.setText('')
        self.encode_letter_u.setText('')
        self.encode_letter_b.setText('')
        self.number_letter.setText('')
        self.source_text_g_var = ''
        self.current_letter = 0

if __name__ == '__main__':
    # Create the Qt Application
    app = QApplication(sys.argv)
    # Create and show the form
    form = Form()
    form.exit_but.clicked.connect(app.quit)
    form.show()

    # Run the main Qt loop
    sys.exit(app.exec_())

